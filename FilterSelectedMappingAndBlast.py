
# coding: utf-8

# Filter Selected Mapping and Blast files
# This scripts filter the probabilistic models hits and blast results to consider completness and redundacy of genomes

import pandas as pd
from os import path

def filter_mapping_and_blast(params_dict):
    """
        :param params_dict: dictionary with parameters
        :return: name of the valid 16S entries filename and blast results to match selected 16S entries to whole
            PATRIC database AFTER filters
        """

    # Read Parameters from dictionary
    PATRIC_GENOMES_SELECTED_MAP = params_dict['INPUT_PATRIC_GENOMES_SELECTED_MAP']
    PATRIC_GENOMES_SELECTED_BLAST = params_dict['INPUT_PATRIC_GENOMES_SELECTED_BLAST']
    CLUSTERS_FILE = params_dict['CLUSTERS_FILE']
    COMPLETENESS_FILE = params_dict['COMPLETENESS_FILE']
    COMPLETENESS_THRESHOLD = params_dict['COMPLETENESS_THRESHOLD']
    PATRIC_GENOMES_SELECTED_BLAST_THRESHOLD = params_dict['PATRIC_GENOMES_SELECTED_BLAST_THRESHOLD']

    #OUTPUT FILES
    PATRIC_PROBABILISTIC_MAP_FILTERED=path.join(params_dict['output_dir'],params_dict['OUTPUT_PATRIC_PROBABILISTIC_MAP_FILTERED'])
    PATRIC_PROBABILISTIC_BLAST_FILTERED=path.join(params_dict['output_dir'],params_dict['OUTPUT_PATRIC_PROBABILISTIC_BLAST_FILTERED'])


    def  check_file(filename):
        print("=== file check: "+filename+"======")
        with open(filename, 'r') as f:
            head = [next(f) for x in xrange(2)]
        for column in head:
            print column[:50]

    #READ DATA
    check_file(PATRIC_GENOMES_SELECTED_MAP)
    check_file(PATRIC_GENOMES_SELECTED_BLAST)
    check_file(CLUSTERS_FILE)
    check_file(COMPLETENESS_FILE)

    patric_map = pd.read_csv(PATRIC_GENOMES_SELECTED_MAP,delimiter=',', comment="#",header=0)
    clusters = pd.read_csv(CLUSTERS_FILE,delimiter=r'\s+',comment="#",header=None, dtype={0:str,1:str})
    clusters.columns=['patric_id','patric_id_rep']
    completeness = pd.read_csv(COMPLETENESS_FILE,delimiter=',', comment="#",header=None, dtype={0:str})
    completeness.columns=['patric_id','completeness']

    # In[3]:

    patric_map['patric_id']=patric_map.taxaid.str.split('-').str[0]


    # ### Step 2 - Merge cluster and completeness data by patric id

    # In[4]:

    patric_map_merged0 = pd.merge(left=patric_map, right=clusters, left_on='patric_id', right_on='patric_id')

    patric_map_merged1 = pd.merge(left=patric_map_merged0, right=completeness, left_on='patric_id', right_on='patric_id', how='left')


    # ### Step 3 - Filter by completness and overrepresentatives (clusters)

    # In[5]:

    patric_map_merged2=patric_map_merged1[(patric_map_merged1['completeness'] >= COMPLETENESS_THRESHOLD) | (patric_map_merged1['completeness'].isnull()) ]

    taxaid_selected=patric_map_merged2.groupby(['otuid','patric_id_rep']).taxaid.transform(max)
    patric_map_merged3=patric_map_merged2.loc[patric_map_merged2.taxaid == taxaid_selected]



    patric_map_clean=patric_map_merged3[['otuid','taxaid','patric_id_rep','completeness']]
    patric_map_clean.columns=['otuid','taxaid_selected','patric_id_rep','completeness']
    patric_map_clean.to_csv(PATRIC_PROBABILISTIC_MAP_FILTERED,index=False)
    check_file(PATRIC_PROBABILISTIC_MAP_FILTERED)


    # ## Blast Section
    # Filter blast hits only in the map file of interest and proceed to further clean up completness and overrepresentation of blaster results.
    #
    # #Per before blast file has 1'134,740 hits for the 2,586 queried taxaids and an average of 438 hits per unique taxa id
    #
    # ### Step 5- Load Blast data

    # In[18]:

    patric_blast = pd.read_csv(PATRIC_GENOMES_SELECTED_BLAST,delimiter=r'\s+',comment="#",header=None).iloc[:,0:3]#, dtype={'patric_id_rep':str})
    patric_blast.columns=['taxaid_queried','taxaid_blasted','score']
    patric_blast['patric_id_queried']=patric_blast.taxaid_queried.str.split('-').str[0]
    patric_blast['patric_id_blasted']=patric_blast.taxaid_blasted.str.split('-').str[0]
    print(len(patric_blast.taxaid_queried))
    print((patric_blast.taxaid_queried.unique().size))


    # ### Step 6 - Check full merge
    # Verify that merging the blast back to the original mapping file finds matches for all 386 otus and 2586 taxaids. It should be an expansion of the prior blast so the overall number of rows should increase

    # In[19]:

    #check only
    patric_blast_in_original_map = pd.merge(left=patric_map[['otuid','taxaid']], right=patric_blast, left_on='taxaid', right_on='taxaid_queried')
    # print(len(patric_blast_in_original_map.otuid))
    # print((patric_blast_in_original_map.otuid.unique().size))
    # print((patric_blast_in_original_map.taxaid.unique().size))


    # ### Step 7 - Filter taxa selected in mapping file
    # Since we have subset the mapping file we should subset the blast file accordingly too. Reduction is significant, about 50% compared with the full size in prior step. 383 OTUs matching 1412 unique (patric) taxaids is preserved from mapping file

    # In[20]:

    patric_blast_in_map = pd.merge(left=patric_map_clean[['otuid','taxaid_selected']], right=patric_blast, left_on='taxaid_selected', right_on='taxaid_queried')
    # print(len(patric_blast_in_map.otuid))
    # print((patric_blast_in_map.otuid.unique().size))
    # print((patric_blast_in_map.taxaid_selected.unique().size))


    # ### Step 8 - Filter by Blast threshold

    # In[21]:

    patric_blast_in_map_thres=patric_blast_in_map[(patric_blast_in_map['score'] >= PATRIC_GENOMES_SELECTED_BLAST_THRESHOLD)]


    # print(len(patric_blast_in_map_thres.otuid))
    # print((patric_blast_in_map_thres.otuid.unique().size))
    # print((patric_blast_in_map_thres.taxaid_selected.unique().size))


    # ### Step 9 - Merge cluster and completeness data by patric id
    # Match clusters and completness

    patric_blast_merged0 = pd.merge(left=patric_blast_in_map_thres, right=clusters, left_on='patric_id_blasted', right_on='patric_id')
    patric_blast_merged0 = patric_blast_merged0.drop('patric_id',axis=1)
    patric_blast_merged0 = patric_blast_merged0.rename(columns={'patric_id_rep':'patric_id_rep_blasted'})

    patric_blast_merged1 = pd.merge(left=patric_blast_merged0, right=completeness, left_on='patric_id_blasted', right_on='patric_id', how='left')
    patric_blast_merged1 = patric_blast_merged1.drop('patric_id',axis=1)
    patric_blast_merged1 = patric_blast_merged1.rename(columns={'completeness':'completeness_blasted'})


    # ### Step 10 - Filter by completness and overrepresentatives (clusters)
    #
    # Filtering per completness is really simple, just checking the completness for the genemes result of the blast process.
    # Filtering overrepresentatives is done by getting selecting a single taxaid_selected (or taxaid_queried) per otu and patric_id_representative genome for the blast results. This garantees that we have an unique set of "good" representative genomes for the next round of work

    # In[24]:

    patric_blast_merged2=patric_blast_merged1[(patric_blast_merged1['completeness_blasted'] >= COMPLETENESS_THRESHOLD) | (patric_blast_merged1['completeness_blasted'].isnull()) ]

    taxaid_selected=patric_blast_merged2.groupby(['otuid','patric_id_rep_blasted']).taxaid_selected.transform(max)
    patric_blast_merged3=patric_blast_merged2.loc[patric_blast_merged2.taxaid_selected == taxaid_selected]

    # print(len(patric_blast_merged3.otuid))
    # print((patric_blast_merged3.otuid.unique().size))
    # print((patric_blast_merged3.taxaid_selected.unique().size))
    # print((patric_blast_merged3.taxaid_blasted.unique().size))
    # print((patric_blast_merged3.patric_id_rep_blasted.unique().size))

    # ### Step 11- Report blast file results
    #
    # Filtering has been completed by creating a subset of the unique patric_id_representative of blasted results per OTU. This is what ultimate is going to be used in the annotation section.

    patric_blast_clean=patric_blast_merged3[['otuid','patric_id_rep_blasted']]
    patric_blast_clean.columns=['otuid','patric_id_rep']
    patric_blast_clean.to_csv(PATRIC_PROBABILISTIC_BLAST_FILTERED,index=False)

    check_file(PATRIC_PROBABILISTIC_BLAST_FILTERED)
    return PATRIC_PROBABILISTIC_MAP_FILTERED, PATRIC_PROBABILISTIC_BLAST_FILTERED




