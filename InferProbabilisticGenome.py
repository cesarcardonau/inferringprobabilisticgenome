# coding: utf-8

# Probabilistic Genome infers genomes from a list of 16S fasta files, green genes and PATRIC databases
# Entails three steps (files)
# 1 FindPatricPangenome
# 2 FilterSelectedMappingAndBlast
# 3 BuildProbabilisticAnnotations
#
from os import path, mkdir, rename
from FindPatricPangenome import find_patric_pangenome
from FilterSelectedMappingAndBlast import filter_mapping_and_blast
from BuildProbabilisticAnnotations import build_probabilistic_annotations
from datetime import datetime

if __name__ == '__main__':
    #### STEP 1 Find Patric Pangenome ####
    # INPUT PARAMETERS
    strdate = datetime.strftime(datetime.today(), '%y%m%d-%H%M')
    print("==== STEP 1 ====")
    print(strdate)
    output_dir="output_results1"
    params_pangenome_dic = dict(input_file="inputfiles/uic_seqs10.fna",
                                output_dir="step1_output",
                                OUTPUT_PATRIC_GENOMES_SELECTED_MAP="patric-16S-selected.map",
                                OUTPUT_PATRIC_GENOMES_SELECTED_BLAST="patric-16S-selected-per-taxaid.blast",
                                PATRIC_GENOMES_FASTA='Databases/patric-16S.fasta',
                                PATRIC_GENOMES_MAP='Databases/patric-16S.taxonomy',
                                GG_GENOMES_FASTA='Databases/gg138_99.fasta',
                                GG_GENOMES_MAP='Databases/gg138_99.taxonomy',
                                MIN_TAXONOMIC_LEVEL=3  # Class
                                )

    # creating the output and intermediate directories early - making sure permissions is not an issue
    output_dir1 = params_pangenome_dic['output_dir']
    if not path.exists(output_dir1):
        mkdir(output_dir1)
    # check if result files already exist therefore skip step
    output_map_file = path.join(output_dir1, params_pangenome_dic['OUTPUT_PATRIC_GENOMES_SELECTED_MAP'])
    output_blast_file = path.join(output_dir1, params_pangenome_dic['OUTPUT_PATRIC_GENOMES_SELECTED_BLAST'])
    if path.exists(output_map_file) and path.getsize(output_map_file) > 0 and path.exists(
            output_blast_file) and path.getsize(output_blast_file) > 0:
        valid_16S_map_filename = output_map_file
        pangenome_blast_filename = output_blast_file
        print("=== result files already exist, skipping STEP 1 ===")
    else:
        print("=== calling STEP 1 find_patric_pangenome fx ===")
        valid_16S_map_filename, pangenome_blast_filename = find_patric_pangenome(params_pangenome_dic)

    #### STEP 2 FilterSelectedMappingAndBlast ####
    strdate = datetime.strftime(datetime.today(), '%y%m%d-%H%M')
    print("==== STEP 2 ====")
    print(strdate)

    params_filter_dic = dict(INPUT_PATRIC_GENOMES_SELECTED_MAP=valid_16S_map_filename,
                             INPUT_PATRIC_GENOMES_SELECTED_BLAST=pangenome_blast_filename,
                             output_dir="step2_output",
                             OUTPUT_PATRIC_PROBABILISTIC_MAP_FILTERED="patric-16S-selected-filtered.map",
                             OUTPUT_PATRIC_PROBABILISTIC_BLAST_FILTERED="patric-16S-selected-filtered.blast",
                             CLUSTERS_FILE="ReferenceData/patric-genomes-clustering_R6.out",
                             COMPLETENESS_FILE='ReferenceData/patric-genomes-completeness-results.csv',
                             COMPLETENESS_THRESHOLD=0.8,
                             PATRIC_GENOMES_SELECTED_BLAST_THRESHOLD=97)

    output_dir2 = params_filter_dic['output_dir']
    if not path.exists(output_dir2):
        mkdir(output_dir2)
    # check if result files already exist therefore skip step
    output_map_file = path.join(output_dir2, params_filter_dic['OUTPUT_PATRIC_PROBABILISTIC_MAP_FILTERED'])
    output_blast_file = path.join(output_dir2, params_filter_dic['OUTPUT_PATRIC_PROBABILISTIC_BLAST_FILTERED'])
    if path.exists(output_map_file) and path.getsize(output_map_file) > 0 and path.exists(
            output_blast_file) and path.getsize(output_blast_file) > 0:
        map_filename_filtered = output_map_file
        pangenome_blast_filename_filtered = output_blast_file
        print("=== result files already exist, skipping STEP 2 ===")
    else:
        print("=== calling STEP 2 filter_mapping_and_blast fx ===")
        map_filename_filtered, pangenome_blast_filename_filtered = filter_mapping_and_blast(params_filter_dic)

    #### STEP 3 BuildProbabilisticAnnotations ####
    strdate = datetime.strftime(datetime.today(), '%y%m%d-%H%M')
    print("==== STEP 3 ====")
    print(strdate)
    params_annotations_dic = dict(INPUT_PATRIC_PROBABILISTIC_BLAST_FILTERED=pangenome_blast_filename_filtered,
                                  output_dir="step3_output",
                                  ANNOTATIONS_DOCUMENT_IDS="ReferenceData/large_document_ids.npy.gz",
                                  ANNOTATIONS_DOCUMENT_CONTENTS="ReferenceData/large_document_contents.npy.gz",
                                  OUTPUT_PROBABILISTIC_ANNOTATIONS="probabilistic-annotations.csv")

    output_dir3 = params_annotations_dic['output_dir']
    if not path.exists(output_dir3):
        mkdir(output_dir3)
    # check if result files already exist therefore skip step
    output_map_file = path.join(output_dir3, params_annotations_dic['OUTPUT_PROBABILISTIC_ANNOTATIONS'])
    if path.exists(output_map_file) and path.getsize(output_map_file) > 0 :
        annotation_file = output_map_file
        print("=== result files already exist, skipping STEP 3 ===")
    else:
        print("=== calling STEP 3 filter_mapping_and_blast fx ===")
        annotation_file = build_probabilistic_annotations(params_annotations_dic)
        strdate = datetime.strftime(datetime.today(), '%y%m%d-%H%M')
        combined_output_dir= output_dir + strdate
        print("Processed finished, archiving results to directory"+ " '"+combined_output_dir+"'")
        if not path.exists(combined_output_dir):
            mkdir(combined_output_dir)
            rename(output_dir1,path.join(combined_output_dir,output_dir1))
            rename(output_dir2,path.join(combined_output_dir,output_dir2))
            rename(output_dir3,path.join(combined_output_dir,output_dir3))
        else:
            print("archiving directory already exist, not directories have been moved")
