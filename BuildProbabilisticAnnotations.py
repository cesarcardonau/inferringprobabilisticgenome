
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from scipy.sparse import csc_matrix
#from collections import Counter
from os import path
import gzip

def build_probabilistic_annotations(params_dict):
    """
        :param params_dict: dictionary with parameters
        :return: file with the PATRIC annotations and fraction seen in pangenome (probability)
        """

    #INPUT FILES
    PATRIC_PROBABILISTIC_BLAST_FILTERED = params_dict['INPUT_PATRIC_PROBABILISTIC_BLAST_FILTERED']
    document_ids=np.load(gzip.GzipFile(params_dict['ANNOTATIONS_DOCUMENT_IDS'], "r"))
    document_contents=np.load(gzip.GzipFile(params_dict['ANNOTATIONS_DOCUMENT_CONTENTS'], "r"))

    #OUTPUT FILE
    PATRIC_PROBABILISTIC_ANNOTATIONS=path.join(params_dict['output_dir'],params_dict["OUTPUT_PROBABILISTIC_ANNOTATIONS"])

    def print_type(var):
        print(type(var).__name__)

    probabilistic_blast = pd.read_csv(PATRIC_PROBABILISTIC_BLAST_FILTERED,delimiter=',',comment="#",header=0, dtype={'otuid':str,'patric_id_rep':str}).iloc[:,0:2]


    print("checks")
    print(len(probabilistic_blast.otuid))
    print((probabilistic_blast.otuid.unique().size))
    print((probabilistic_blast.patric_id_rep.unique().size))


    doc_hash={k:v for k, v in zip(document_ids, document_contents)}
    print(len(doc_hash))
    #check specific documents
    #doc_test_list=doc_hash['1268078.3'].split(",")
    #print(doc_test_list)
    #c=Counter(doc_test_list)
    # print(c[' 10'])
    # print(c[' 100'])
    # print(c[' 431'])
    # print(len(document_ids),document_ids[0:1])
    # print(len(document_contents),np.array_str(document_contents[0:1])[0:100])


    # ### Step 3 - Filter document ids present in the blast file only

    doc_hash_filtered={k: doc_hash[k] for k in probabilistic_blast.patric_id_rep.tolist()}
    print("length of doc_hash_filtered {}".format(len(doc_hash_filtered)))


    # ### Step 4 - Use text mining tool to create counts table of each annotation for each patric id

    vec = CountVectorizer()
    print_type((doc_hash_filtered.values()))
    print_type((doc_hash_filtered.values()[0]))
    annotation_counts = csc_matrix(vec.fit_transform(doc_hash_filtered.values()))


    # #Checks
    print("annotation_counts shape {}".format(annotation_counts.shape))
    # print(annotation_counts.A[0:20,0:30])
    # colnamesX=vec.get_feature_names()
    # print(colnamesX[0:20])
    # print(doc_hash_filtered.keys()[0:20])
    # indices = [i for i, elem in enumerate(colnamesX) if '431' == elem]
    # print(indices)
    # print(annotation_counts.A[0:20,73052])


    # ### Step 5 -  Updating counts with value of 1 for ease of next steps - we are only considering presence/absence
    #print(annotation_counts.A[0:20,73052])
    annotation_counts.data[:] = 1
    #print(annotation_counts.A[0:20,73052])


    # ### Step 6 - Create a dataframe for easier summarization
    prob_mat = pd.DataFrame(annotation_counts.toarray(), index=doc_hash_filtered.keys(), columns=vec.get_feature_names())

    print("prob_mat shape {}".format(prob_mat.shape))


    # ### Step 7 - merge counts table with probabilist mapping table
    # This expands tha file to the OTU level so some duplicates of patric_ids are added.

    prob_mat_merged = pd.merge(left=probabilistic_blast,right=prob_mat, left_on='patric_id_rep', right_index=True)


    print("prob_mat_merged shape {}".format(prob_mat_merged.shape))

    # print(prob_mat_merged.shape)
    # print((prob_mat_merged.otuid.unique().size))
    # print((prob_mat_merged.patric_id_rep.unique().size))
    # display(prob_mat_merged.shape,prob_mat_merged.iloc[0:5,range(10)+[73052+2]])


    # ### Step 8 - check spread of patric matches for each otu

    #print(prob_mat_merged.groupby('otuid')['patric_id_rep'].count()[0:10])
    print(prob_mat_merged.groupby('otuid')['patric_id_rep'].count().describe())


    # ### Step 9 - run mean annotations per otu for whole dataset and save to flat file

    prob_mat_mean=prob_mat_merged.groupby('otuid')[vec.get_feature_names()].mean()


    prob_mat_flat=pd.DataFrame(prob_mat_mean.T.unstack().reset_index())
    prob_mat_flat.columns=['otuid','function','probability']

    prob_mat_flat[prob_mat_flat.probability>0].to_csv(PATRIC_PROBABILISTIC_ANNOTATIONS,mode='w',index=False)
    return PATRIC_PROBABILISTIC_ANNOTATIONS

