Latest update: November 10 2019

Requires python 2.7

# Infer Probabilistic Genomes
** Infer Probabilistic Genome infers genomes from a list of 16S fasta files, green genes and PATRIC databases**
Code for predicting probabilistic genomes based on 16S markers and PATRIC database. 
Will feed into KBASE http://www.kbase.us/ for ** probabilistic models** .

Full details and working document https://goo.gl/fBmkcm

### What is new?
Moving away from notebooks to python scripts

Main script `InferProbabilisticGenome.py` calls three steps

1 FindPatricPangenome

2 FilterSelectedMappingAndBlast

3 BuildProbabilisticAnnotations

### Who do I talk to?

Gilbert Lab http://www.gilbertlab.com/

Henry Lab at Argonne National Labs

Cesar Cardona cesarcardonau AT uchicago.edu
