# coding: utf-8
# #### Developed in Python 2.7
# #### Cesar Cardona -  20181211
# #### 20180209 - v3 base version
# #### 20181211 - v4 changing the min_taxonomic_level and rerun
# #### 20191110 - v5 making it a py file
# 
# The goal of this code is to find the most similar (best matching) set of 16S genomes on the Patric database (pangenome)
# for a given 16S query genome (or genomes), in a way that we can infer that such set will contain very likely the same full genome features of the queried genome. The ratio of genes found on the set will give us a probability to be found in them query.
# Technically this code could easily be extended beyond 16S to any other marker gene that has a similar databases organization available. This code takes an input fasta file with multiple query 16S genomes, its name is defined by CENTROID_FASTA and the ultimate result is store in the file with name defined by the variable PATRIC_GENOMES_SELECTED_BLAST

from Bio import SeqIO
from os import path
import subprocess
import pandas as pd

def find_patric_pangenome(params_dict):
    """
    Identify entries from the 16S (OTU) aka. CENTROID_FASTA
    :param params_dict: dictionary with parameters
    :return: name of the valid 16S entries filename and blast results to match selected 16S entries to whole
        PATRIC database
    """

    #Read Parameters from dictionary
    CENTROID_FASTA=params_dict['input_file']
    PATRIC_GENOMES_FASTA=params_dict['PATRIC_GENOMES_FASTA']
    PATRIC_GENOMES_MAP=params_dict['PATRIC_GENOMES_MAP']
    GG_GENOMES_FASTA=params_dict['GG_GENOMES_FASTA']
    GG_GENOMES_MAP=params_dict['GG_GENOMES_MAP']

    #INTERMEDIATE FILES FOR STEP 2 - BLAST TO GG AND PATRIC DB
    CENTROIDS_GG_BLAST=path.join(params_dict['output_dir'],"bacteria-centroids.gg.blast")
    CENTROIDS_PATRIC_BLAST=path.join(params_dict['output_dir'],"bacteria-centroids.patric.blast")

    #OUTPUT FILES
    PATRIC_GENOMES_SELECTED_MAP=path.join(params_dict['output_dir'],params_dict['OUTPUT_PATRIC_GENOMES_SELECTED_MAP'])
    PATRIC_GENOMES_SELECTED_FASTA=path.join(params_dict['output_dir'],'patric-16S-selected-per-taxaid.fasta')
    PATRIC_GENOMES_SELECTED_BLAST=path.join(params_dict['output_dir'],params_dict['OUTPUT_PATRIC_GENOMES_SELECTED_BLAST'])#Pangenome

    MIN_TAXONOMIC_LEVEL=params_dict['MIN_TAXONOMIC_LEVEL']
    MAX_TOP_RANK_GG_SELECTED=1 #Only the top 1 GG hit is consider groud truths

    def  check_file(filename):
        print("=== file check: "+filename+"======")
        with open(filename, 'r') as f:
            head = [next(f) for x in xrange(2)]
        for column in head:
            print column[:50]

    def run_blastn90_30(fp_query,db_name,fp_out):
        blastn_cmd='blastn -query {0} -max_target_seqs 30 -strand both -task blastn -db {1} -perc_identity 90 -outfmt 7 -out {2}'.format(fp_query,db_name,fp_out )
        print("=== starting blast with command:" + blastn_cmd)
        BLAST_process = subprocess.call(blastn_cmd,shell=True)
#        BLAST_process.wait()
        return(fp_out)
    def run_blastn95(fp_query,db_name,fp_out):
        blastn_cmd='blastn -query {0} -strand both -task blastn -db {1} -perc_identity 95 -outfmt 7 -out {2}'.format(fp_query,db_name,fp_out )
        print("=== starting blast with command:" + blastn_cmd)
        BLAST_process = subprocess.call(blastn_cmd,shell=True)
#        BLAST_process.wait()
        return(fp_out)

    #[13]:

    ##STEP 1 CHECK INPUT FILES
    check_file(CENTROID_FASTA)
    check_file(GG_GENOMES_FASTA)
    check_file(GG_GENOMES_MAP)
    check_file(PATRIC_GENOMES_FASTA)
    check_file(PATRIC_GENOMES_MAP)

    # ### SECTION 1
    # Blast centroids (query data) to patric and green genes databases

    # STEP 2 - WARNING 3H PROCESS
    # BLAST YOUR FILE TO BOTH GG AND PATRIC 16S
    print(run_blastn90_30(CENTROID_FASTA,GG_GENOMES_FASTA,CENTROIDS_GG_BLAST))
    check_file(CENTROIDS_GG_BLAST)
    print(run_blastn90_30(CENTROID_FASTA,PATRIC_GENOMES_FASTA,CENTROIDS_PATRIC_BLAST))
    check_file(CENTROIDS_PATRIC_BLAST)

    # STEP 3 - LOAD TAXONOMY FILES
    gg_map = pd.read_csv(GG_GENOMES_MAP,delimiter=r'\s+', comment="#",header=None)
    gg_map.columns=['taxaid','gg_taxa']

    patric_map = pd.read_csv(PATRIC_GENOMES_MAP,delimiter=r'\s+', comment="#",header=None)
    patric_map.columns=['taxaid','patric_taxa']

    # STEP 4 - LOAD BLAST RESULTS FOR BOTH DATABESES
    gg_blast = pd.read_csv(CENTROIDS_GG_BLAST,delimiter=r'\s+', comment="#",header=None).iloc[:,0:3]
    gg_blast.columns=['otuid','taxaid','score']
    patric_blast = pd.read_csv(CENTROIDS_PATRIC_BLAST,delimiter=r'\s+', comment="#",header=None).iloc[:,0:3]
    patric_blast.columns=['otuid','taxaid','score']

    #STEP 5 ATTACH TAXONOMY TO THE BLAST RESULTS
    gg_res = pd.merge(left=gg_blast,right=gg_map, left_on='taxaid', right_on='taxaid')
    patric_res = pd.merge(left=patric_blast,right=patric_map, left_on='taxaid', right_on='taxaid')


    # ### SECTION 2
    # Identify otus that have higher blast score in green gene database while also satisfying the minimum taxonomic level requirement. We only want matches where the taxonomy of the 16S is defined up to a minimum level (MIN_TAXONOMIC_LEVEL) Pick the higher scoring match that still satisfy the min taxonomic level. These `selected` taxonomies are used as ground truth to compare against in future steps

    #STEP 6 FIND BEST GG MATCH
    #Selects only the top MAX_TOP_RANK_GG_SELECTED top scores with the full taxonomic level to at least MIN_TAXONOMIC_LEVEL
    #from gg database to be the groud truth of future comparisons (patric 16S filtering)
    #print(gg_res.loc[690:700,'gg_taxa'])
    gg_taxa_split=[filter(None,x) for x in gg_res.loc[:,'gg_taxa'].str.split(";")]
    gg_taxa_counts=[len(x) for x in gg_taxa_split]
    #print(gg_taxa_counts[690:700])
    gg_res['best_taxonomic_match']=gg_taxa_counts
    #select only the records with at least the MINIMUM_TAXONOMIC_LEVEL taxonomic level (class=3,order=4,familiy=5)
    gg_res_good_taxa=gg_res[gg_res.best_taxonomic_match >= MIN_TAXONOMIC_LEVEL].copy()
    #Rank the scores and dense score will assign the same high score in case of a tie
    gg_res_good_taxa['score_ranked']=gg_res_good_taxa.groupby("otuid")['score'].rank(ascending=0,method='dense')
    #print(gg_res_good_taxa.loc[690:700])
    #Finally keeping only the MAX_TOP_RANK_GG_SELECTED, which may includes ties
    gg_res_valid=gg_res_good_taxa[gg_res_good_taxa.score_ranked <= MAX_TOP_RANK_GG_SELECTED]
    #print(gg_res_valid.loc[690:700])

    #STEP 7 MATCH GG matches with PATRIC HITS
    #For each centroid match, check consistency of PATRIC hits, select higher score patric hit that matches the best hit
    # from GG database. This could lead to multiple hits or none.
    #for each otu in patric loop
    patric_best_list=[]
    example_counter=0
    EXAMPLE_MAX=0
    for otu_current in patric_res['otuid'].unique():
        if(example_counter<EXAMPLE_MAX):
            print(otu_current)
        #find any matching otus in gg
        gg_taxa_current=gg_res_valid[(gg_res_valid.otuid==otu_current)].loc[:,'gg_taxa'].str.split(";")
        #if match is found move on otherwise continue the loop
        if len(gg_taxa_current) > 0 :
            #add gg taxonomies to a hash with the name and position on the taxonomy Kingdom=1 Specie=7
            gg_hash_list=[]
            for taxa_current in gg_taxa_current:
                taxa_clean=filter(None, taxa_current)
                gg_pos=range(1,len(taxa_clean)+1)
                gg_hash_list.append({k:v for k, v in zip(taxa_clean, gg_pos)})
            #final hash of taxonomies found
            gg_hash=dict(pair for d in gg_hash_list for pair in d.items())
            if(example_counter<EXAMPLE_MAX):
                print(gg_hash)
            #now match the patric taxonomies to our gg_hash, find max match (higher order)
            patric_filt=patric_res[(patric_res.otuid==otu_current)].copy()
            max_match_list=[]
            for pos in patric_filt.index.values:
                taxa_split=patric_filt.loc[pos,].patric_taxa.split(";")
                matches=[]
                for taxa_element in taxa_split:
                    if taxa_element in gg_hash:
                        matches.append(gg_hash[taxa_element])
                if len(matches) > 0:
                    max_match_list.append(max(matches))
                else:
                    max_match_list.append(0)
            if(example_counter<EXAMPLE_MAX):
                print(max_match_list)
            example_counter += 1
            #add max_match for each row
            patric_filt['best_taxonomic_match']=max_match_list
            #this gets the max score with at least a match at the MINIMUM_TAXONOMIC_LEVEL taxonomic level (family=5)
            patric_valid=patric_filt[patric_filt.best_taxonomic_match >= MIN_TAXONOMIC_LEVEL]
            if patric_valid.empty == False:
                max_score=max(patric_valid.score)
                #get all rows with maxscore
                patric_best_list.append(patric_valid[patric_valid.score==max_score])
    patric_best=pd.concat(patric_best_list)

    #STEP 8. FROM THE ORIGINAL OTUS (16S) we identify the best PATRIC matches, a PATRIC match could be associated with one
    # or more request. The unique list of unique PATRIC ids will be saved for future blast
    patric_best[['otuid','taxaid','score']].to_csv(PATRIC_GENOMES_SELECTED_MAP,index=False)


    #[30]:

    infile = open(PATRIC_GENOMES_FASTA)
    outfile = open (PATRIC_GENOMES_SELECTED_FASTA, "w")
    selected_seqs=[]
    for record in SeqIO.parse(infile, "fasta"):
        taxaid = record.id.strip()
        if((taxaid in patric_best.taxaid.values)):
            selected_seqs.append(record)
    SeqIO.write(selected_seqs, PATRIC_GENOMES_SELECTED_FASTA, "fasta")
    infile.close()
    outfile.close()

    # ### SECTION 3
    # Create PANGENOME blasting selected patricids to patric database
    # Blast `patric_best` taxaids and produce file named by variable PATRIC_GENOMES_SELECTED_BLAST that is used
    # to match against all other PATRIC 16S database and define the pangenome

    #STEP 9 WARNING UP TO 24 HOURS EXECUTION!!!
    print(run_blastn95(PATRIC_GENOMES_SELECTED_FASTA,PATRIC_GENOMES_FASTA,PATRIC_GENOMES_SELECTED_BLAST))

    patric_selected_blast = pd.read_csv(PATRIC_GENOMES_SELECTED_BLAST,delimiter=r'\s+', comment="#",header=None).iloc[:,0:3]
    patric_selected_blast.columns=['taxaid_queried','taxaid_blasted','score']
    return PATRIC_GENOMES_SELECTED_MAP,PATRIC_GENOMES_SELECTED_BLAST
